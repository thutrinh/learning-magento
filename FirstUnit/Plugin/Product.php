<?php

namespace Learning\FirstUnit\Plugin;

class Product
{
    public function afterGetPrice(\Magento\Catalog\Model\Product $subject, $result)
    {
        return $result + 0.25;
    }
}