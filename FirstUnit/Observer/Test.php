<?php


namespace Learning\FirstUnit\Observer;

use Magento\Framework\Event\ObserverInterface;


class Test implements ObserverInterface
{
    protected $logger;

    public function __construct(
        \Psr\Log\LoggerInterface $logger
    )
    {
        $this->logger = $logger;
        // Observer initialization code...
        // You can use dependency injection to get any class this observer may need.
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $this->logger->info("TEST LOG: ".$observer->getEvent()->getName());
    }
}