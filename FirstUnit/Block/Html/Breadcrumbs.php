<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Learning\FirstUnit\Block\Html;


class Breadcrumbs extends \Magento\Theme\Block\Html\Breadcrumbs
{
    public function addCrumb($crumbName, $crumbInfo)
    {
        return parent::addCrumb($crumbName . '(!)', $crumbInfo);
    }
}